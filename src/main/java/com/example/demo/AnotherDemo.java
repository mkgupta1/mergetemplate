package com.example.demo;


import java.util.Objects;

public class AnotherDemo {

    public AnotherDemo(double salary) {
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnotherDemo that = (AnotherDemo) o;
        return Double.compare(that.salary, salary) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(salary);
    }

    double salary;


    public double creditmysalary() {
        return salary;
    }

    public static void display() {
        System.out.println("helo");
    }

}
